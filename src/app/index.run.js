export function runApi(Restangular, $rootScope, localStorageService) {
  Restangular.setErrorInterceptor(function (response) {
    console.assert(response.status, `Сервер умер или проблемма с настройкой CORS
        (На сервер не разрешены запросы с домена: ${window.location.origin})`);
    try {
      // Приводим в правильный вид ответ с ошибками
      // В ответе может быть message: String   message: Array  message: Object
      if (angular.isString(response.data.message)) {
        response.data.message = {'general_api_error': [response.data.message, 0]};
      }
      if (angular.isArray(response.data.message)) {
        response.data.message = {'general_api_error': response.data.message};
      }
      // перехватываем неавторизированных пользователей
      if (response.status === 401) {
        localStorageService.set('user.is_authenticated', 0);
        localStorageService.set('user.data', {});

        // Обработать не авторизованый запрос
        $rootScope.$emit('login_required');
        return false; // error handled
      }
    } catch (ev) {
    }


    return true; // error not handled
  });
}

export function runScroll($rootScope, $window, config) {
  // Автоматичеки выставляем скролл на верх страници
  $rootScope.$on('$stateChangeSuccess', function (e, state) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $window.document.title = (state.data) ? state.data.pageTitle : '';
  });

  // Для регистрации через социальную сеть в модалках
  $rootScope.apiUrl = config.api_url;
}
