export function routeConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      template: 'home'
    })
  ;

  $urlRouterProvider.otherwise('/');
}
routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
