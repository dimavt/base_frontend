import {AppCtrl} from './app.ctrl.js';

function Application() {
  return {
    restrict: 'E',
    template: require('./app.html'),
    controller: AppCtrl,
    controllerAs: 'vm'
  };

}

export default ()=> new Application();
