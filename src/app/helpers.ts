export function mockResponse<T>(res:T, delay = 1000):angular.IPromise<T> {
  let api = angular.element(document).injector().get('$q');
  let deferred = api.defer();
  setTimeout(()=>deferred.resolve(res), delay);
  return deferred.promise;
}
