import * as angular from 'angular';
import 'styles/index.css';

// angular-material css
import '!!style!css!angular-material/angular-material.css';


import config from './conf';
import {jwtAuthConfig, restangularConfig} from './index.config';
import {routeConfig} from './index.route';
import {runApi, runScroll} from './index.run';

import App from './main/app.cmp';

require('expose?_!lodash');
require('restangular');
require('angular-local-storage');

// app для верстки
import SlicingApp from './slicing/slicing.app';

angular.module(APP_NAME, [
    SlicingApp,
    require('angular-messages'),
    require('angular-ui-router'),
    require('angular-jwt'),
    require('angular-material'),
    'restangular',
    'LocalStorageModule'
  ])
  .constant('config', config)
  .directive('app', App)
;


if (!ANGULAR2) {
  angular.element(document).ready(function () {
    angular.bootstrap(document, [APP_NAME]);
  });
} else {
  let adapter = require('./upgrade.ts').default;
  adapter.bootstrap(document.documentElement, [APP_NAME]);
}
