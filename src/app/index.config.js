export function jwtAuthConfig($httpProvider, jwtInterceptorProvider) {
  jwtInterceptorProvider.tokenGetter = function (jwtHelper, $http, localStorageService) {
    var idToken = localStorageService.get('id_token');
    var refreshToken = localStorageService.get('refresh_token');

    if (idToken && jwtHelper.isTokenExpired(idToken)) {
      // This is a promise of a JWT id_token
      return $http({
        url: '',
        // This makes it so that this request doesn't send the JWT
        skipAuthorization: true,
        method: 'POST',
        data: {
          grant_type: 'refresh_token',
          refresh_token: refreshToken
        }
      }).then(function (response) {
        var id_token = response.data.id_token;
        localStorageService.set('id_token', id_token);
        return id_token;
      });
    } else {
      return idToken;
    }
  }
  ;
  jwtInterceptorProvider.tokenGetter.$inject = ['jwtHelper', '$http', 'localStorageService'];
  $httpProvider.interceptors.push('jwtInterceptor');
}

export function restangularConfig(RestangularProvider, config) {
  'ngInject';
  RestangularProvider.setBaseUrl(config.api_url);
  RestangularProvider.setDefaultHeaders({'X-Frontend-Url': window.location.origin});
  RestangularProvider.setRestangularFields({
    id: '_id'
  });
  RestangularProvider.setResponseExtractor(function (response, operation) {
    var newResponse;
    if (operation === 'getList') {
      newResponse = response.objects;
      newResponse.total = response.total;
    } else {
      newResponse = response;
    }
    return newResponse;
  });

}
