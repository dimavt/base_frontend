/*jshint latedef:false */
import './styles.css';

export default function () {
  return {
    scope: {},
    restrict: 'E',
    template: require('./template.html'),
    controller: ComponentComponent,
    controllerAs: 'vm',
  };
}
class ComponentComponent {
  constructor(){
    // Add some logic
  }
}
