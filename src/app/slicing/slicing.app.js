// slicing helper application v 0.0.3
import * as angular from 'angular';

// Helpers
var SPECIAL_CHARS_REGEXP = /([\:\-\_]+(.))/g;
var MOZ_HACK_REGEXP = /^moz([A-Z])/;
function toCamelCase(str) {
  return str.replace(SPECIAL_CHARS_REGEXP, function (_, separator, letter, offset) {
    return offset ? letter.toUpperCase() : letter;
  }).replace(MOZ_HACK_REGEXP, 'Moz$1');
}

function camelToDash(str) {
  return str.replace(/\W+/g, '-')
    .replace(/([a-z\d])([A-Z])/g, '$1-$2')
    .toLowerCase();
}

let components = require.context('./', true, /.*\.cmp\.js$/).keys();
let apps = components.map(location=> {
  let name = location.split('/')[1];
  let camelName = toCamelCase(name);
  let dashName = camelToDash(camelName);

  return angular.module(camelName + 'Slicing', [
      require('angular-ui-router'),
    ])
    .directive(camelName + 'Slicing', require(location).default)
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state(dashName + 'Slicing', {
          url: '/sl/' + dashName,
          template: `<${dashName}-slicing></${dashName}-slicing>`
        });
    }]).name;
});

export default angular.module('slicingApp', apps).name;




