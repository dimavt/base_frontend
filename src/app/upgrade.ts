const angularPoehali = `
npm i --save angular2
npm i --save es6-promise
npm i --save es6-shim@^0.33.3
npm i --save reflect-metadata
npm i --save rxjs
npm i --save zone.js
`;


import 'reflect-metadata';
//noinspection TypeScriptCheckImport
import {UpgradeAdapter} from 'angular2/upgrade';
const adapter = new UpgradeAdapter();
export default adapter;
