'use strict';
let path = require('path');
var fs = require('fs');

let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

let rimraf = require('rimraf');

const NODE_ENV = process.env.NODE_ENV || 'development';
const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 8080;
const context = path.join(__dirname, 'src');

function module_exists(name) {
  try {
    return require.resolve(name)
  }
  catch (e) {
    return false
  }
}

// Общие опции
let options = {
  context: context,

  // target: 'web', // Для какой платформы собираем модуль

  // Точки входа
  entry: [
    './app/index.module.js',
    // './src/index.html'
  ],
  // Итоговые файлы
  output: {
    path: path.join(__dirname, 'dist'), // Путь итоговых файлов
    publicPath: '/',                    // Внешний путь
    filename: '[name].[hash:6].js',     // Имена итоговых файлов
    library: '[name]'
  },

  resolve: {
    modulesDirectories: ['node_modules', 'bower_components', context], // Директории поиска модулей (если не указаны)
    extensions: [ // Расширения по умолчанию (если не указаны)
      '.ts',
      '.js',
      '.json',
      '.css',
      '.html',
      ''
    ]

  },

  resolveLoader: {
    modulesDirectories: ['node_modules'], // Директории поиска loaders (если не указаны)
    modulesTemplates: ['*-loader', '*'], // Директории поиска loaders (если не указаны)
    extensions: ['', '.js'] // Расширения по умолчанию для loaders (если не указаны)
  },

  // =====================================
  module: {
    preLoaders: [],

    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        query: {
          'ignoreDiagnostics': [
            2403, // 2403 -> Subsequent variable declarations
            2300, // 2300 -> Duplicate identifier
            2374, // 2374 -> Duplicate number index signature
            2375  // 2375 -> Duplicate string index signature
          ]
        },
        exclude: [/\.(spec|e2e)\.ts$/, /node_modules/]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel?presets[]=es2015'
      },
      {
        test: /\.css$/,
        //exclude: /node_modules/,
        loader: "style!css!postcss"
        //loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader")
      },
      {
        test: /\.html$/,
        // exclude: /node_modules/,
        loader: 'html' // https://github.com/webpack/html-loader
      },
      {
        test: /\.json/,
        exclude: /node_modules/,
        loader: "json"
      },
      {
        test: /\.(png|jpg|svg)$/,
        exclude: /node_modules/,
        loader: 'file?name=assets/images/[name].[hash:6].[ext]'
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        exclude: /node_modules/,
        loader: 'file?name=assets/fonts/[name].[hash:6].[ext]'
      },

      // { test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff' },
      // { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream' },
      // { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
      // { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' }
    ]
  },

  // =====================================
  plugins: [
    // Очиска дериктории dist перед билдом
    {apply: (compiler) =>  rimraf.sync(compiler.options.output.path)},
    // Игнорируем локаль moment js
    //new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),

    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'body'
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      //moment: 'moment/moment'
    }),
    // Пробрасываем значение переменной окружения NODE_ENV в js код
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      APP_NAME: JSON.stringify(require('./package.json').name || 'app'),
      ANGULAR2: require('./package.json').dependencies.hasOwnProperty('angular2')
    }),

    new CopyWebpackPlugin(
      fs.readdirSync(context)
        .filter(file=> fs.statSync(path.join(context, file)).isFile()) // возможно есть более удачный вариант проверки
        .map(name=>({from: path.join(context, name)}))
    )
  ],

  noParse: [
    /[\/\\]node_modules[\/\\]angular[\/\\]angular\.js$/  // Не прогонять большие либы через билд
  ],

  // =====================================


  // PostCSS опции
  postcss: function (webpack) {
    return {
      defaults: [
        require('postcss-import')({
          addDependencyTo: webpack
        }),
        require('postcss-url'),
        require('autoprefixer'),
        require('postcss-nested-props'),
        require('precss'),
      ]
    };
  }
};

// =====================================

if (NODE_ENV == 'development') {
  options.watch = true;
  options.watchOptions = {
    aggregateTimeout: 100
  };
  options.devServer = {
    devtool: "source-map",
    contentBase: "dist",
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunk: false
    },
    host: HOST,
    port: PORT
  };
  // TODO Сделать возможным использовать
  //options.plugins.push(new ExtractTextPlugin('[name].css'));
  // магические подключения через regexp для верстальщиков кричат о том что это магия
  options.module.exprContextCritical = false;
  options.output.filename = '[name].js';
  if (process.env.NODE_PROFILE) {
    options.debug = true; // Переключить loaders в режим отладки.
    options.profile = true; // Отображать статистику загрузки модулей http://webpack.github.io/analyse/
  }
} else if (NODE_ENV == 'production') {
  options.plugins.push(new webpack.optimize.OccurenceOrderPlugin());
  options.plugins.push(new webpack.optimize.DedupePlugin());
  options.plugins.push(new webpack.optimize.UglifyJsPlugin({output: {comments: false}}));
  options.plugins.push(new ExtractTextPlugin('[name].[hash:6].css'));
  options.plugins.push(new webpack.NoErrorsPlugin());
}

if (module_exists('open-browser-webpack-plugin')) {
  options.plugins.push(new (require('open-browser-webpack-plugin'))({url: 'http://' + HOST + ':' + PORT + options.output.publicPath}));
}

if (module_exists('jshint-loader')) {
  options.module.preLoaders.push({
    test: /\.js$/,
    exclude: /node_modules|bower_modules/,
    loader: 'jshint'
  });
}

if (module_exists('tslint-loader')) {
  options.module.preLoaders.push({
    test: /\.ts$/,
    exclude: /node_modules|bower_modules/,
    loader: 'tslint'
  });
}

module.exports = options;
